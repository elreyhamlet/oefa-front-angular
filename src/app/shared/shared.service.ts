import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FilesAlfresco } from '../interface/files.interface';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  pathFIle = 'https://api.cloudinary.com/v1_1/dw31wlkgj/image/upload';

  constructor(public http: HttpClient,
  ) { }
  getQuery(query: string, headers: HttpHeaders) {
    const url = `${environment.URL_SERVICIO}/${query}`;
    return this.http.get(url, { headers });

  }
  postQuery<T>(query: string, body: any, headers: HttpHeaders) {
    const url = `${environment.URL_SERVICIO}/${query}`;
    return this.http.post<T>(url, body, { headers });

  }

  loadFile(fileData: FormData, path: string) {
    return this.http.post<any>(path, fileData);
  }

  validarArchivoPermitido(file: File, files: FilesAlfresco[]) {
    const sizeFIle = file.size;
    const fileSizeMB = Math.round(sizeFIle / (1024 * 1024));
    const extensionesPermitidas: string[] = [
      '.pdf',
      '.xpdf',
      '.doc',
      '.docx',
      '.xls',
      '.xlsx',
      '.mp4',
      '.mp3',
      '.jpg',
      '.zip',
      '.rar',
      '.dwg',
      '.ppt',
      '.pptx',
      '.png',
    ];
    const alfresco: FilesAlfresco = { name: '', size: '', status: false, uuid: '' };
    const resultado = {
      estado: false,
      mensaje: '',
      payload: alfresco
    };

    if (sizeFIle <= 0) {
      resultado.mensaje = 'El peso de archivo es muy pequeño, revise que muestre contenido o no esté dañado';
      return resultado;
    }
    if (fileSizeMB > 1024) {
      resultado.mensaje = 'El tamaño máximo es de 1gb. revisa el tamaño de los archivos que intenta cargar.';
      return resultado;
    }

    const nombreFile = file.name.toLowerCase();
    const extensionFile = nombreFile.substring(nombreFile.lastIndexOf('.'));
    const encontrado = extensionesPermitidas.find((r) => r === extensionFile);
    console.log(encontrado)
    if (encontrado === undefined) {
      resultado.mensaje = 'Los formatos permitidos son .pdf, .xpdf, .doc, .docx, .xls, .xlsx, .mp4, .jpg, .png, .zip, .rar, dwg, .mp3 revisa el formato del archivo que intenta cargar';
      return resultado;
    }

    for (const item of files) {
      const fileAgregado = item.name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '_');
      const filePorAgregar = file.name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '_');

      if (fileAgregado === filePorAgregar) {
        resultado.mensaje = 'El archivo ya se encuentra en la lista, seleccione otro archivo.';
        return resultado;
      }
    }

    let fileNameArray = file.name.split('.');
    let realFileName = '';
    let fileSize;
    for (let i = 0; i < fileNameArray.length - 1; i++) {
      realFileName += fileNameArray[i];
    }
    realFileName = realFileName.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '_');
    if (fileSizeMB <= 0) {
      fileSize = Math.round(file.size / 1024) + ' KB'
    } else {
      fileSize = fileSizeMB + 'MB';
    }

    const fileAlfresco: FilesAlfresco = {
      name: realFileName + '.' + fileNameArray[fileNameArray.length - 1],
      size: fileSize,
      status: true,
      uuid: ''
    };
    resultado.estado = true;
    resultado.payload = fileAlfresco;
    return resultado;
  }

  get getToken() {
    const token = localStorage.getItem('sgea-token-cc-wapo');
    return token ? token : '';
  }

}
