
export interface RpeaResponse {
    estado: boolean;
    status: number;
    mensaje: string;
    payload: Rpea;
}

export interface Rpea {
    administradoRpea: AdministradoRpea;
    eventoRpea: EventoRpea;
    origenRpea: OrigenRpea;
}

export interface AdministradoRpea {
    unidadFiscalizable: string;
    idUnidadFiscalizable: number;
    flagUnidadFiscalizable: boolean;
    subSector: string;
    idSubSector: number;
    contacto: Contacto[];
    listaArchivo: ListaArchivo[];
    detalleEvidencia: string;
}

export interface Contacto {
    nombreContacto: string;
    correoContacto: string;
    telefonoContacto: string;
}

export interface ListaArchivo {
    uuidArchivo: string;
    nombreArchivo:string;
}

export interface EventoRpea {
    nombreInstalacion: string;
    fechaEvento: string;
    horaInicioEvento: string;
    horaFinEvento: string;
    areaAfectada: string;
    unidadAreaAfectada: string;
    idUnidadAreaAfectada: number;
    cantidadInvolucrada: string;
    unidadCantidadInvolucrada: string;
    idCantidadInvolucrada: number;
    descripcionLugar: string;
    coordenadas: Coordenadas;
    idUbigeo: string;
    localidad: string;
    zona: string;
}

export interface Coordenadas {
    coordenadaEste: string;
    coordenadaNorte: string;
    zonaLetra: string;
    zonaNumero: string;
    longitud: string;
    latitud: string;
}

export interface Ubigeo {
    departamento: string;
    idDepartamento: number;
    provincia: string;
    idprovincia: number;
    distrito: string;
    idDistrito: number;
}

export interface OrigenRpea {
    origenEA: OrigenEA;
    componenteEA: ComponenteEA;
    descripcionEvento: string;
    flagPlanContingencia: boolean;
    descripcionPlanContigencia: string;
}

export interface ComponenteEA {
    aguaFlag: boolean;
    sueloFlag: boolean;
    aireFlag:boolean;
    otrosFlag:boolean;
    componenteDescripcion:string;
}

export interface OrigenEA {
    climaticoFlag: boolean;
    tecnologicoFlag: boolean;
    humanoFlag: boolean;
    terceroFlag: boolean;
    otrosFlag: boolean;
    origenDescripcion: string;
}

