import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EstadoEA, EstadoEAResponse } from '../../interface/estadoEA.interface';
import { EstadoEAService } from '../../services/estado-ea.service';

@Component({
  selector: 'app-estado-emergencia-ambiental',
  templateUrl: './estado-emergencia-ambiental.component.html',
  styleUrls: ['./estado-emergencia-ambiental.component.css']
})
export class EstadoEmergenciaAmbientalComponent implements OnInit {

  estadoEAList: EstadoEA[] = [];
  img: string[] = [
    'assets/img/img1.png',
    'assets/img/img2.png',
    'assets/img/img3.png',
    'assets/img/img4.png',
    'assets/img/img5.png'];
  codigoEA: string = '';
  constructor(private ea: EstadoEAService, private activated: ActivatedRoute) { }

  ngOnInit() {
    this.activated.params.subscribe((res) => {

      this.listarEstadoEA(res.id);
    });

  }

  listarEstadoEA(codigoEA: string) {
    this.ea.listarEstadosEA({ eaId: codigoEA }).subscribe((res: EstadoEAResponse) => {
      if (res.estado) {
        this.estadoEAList = res.payload.map((r, i) => {
          return {
            ...r,
            path: this.img[i]
          }
        });
     //   this.estadoEAList = this.estadoEAList.reverse();
        this.codigoEA = this.estadoEAList[0]?.codigoEmergencia;
      }

    });
  }

}
