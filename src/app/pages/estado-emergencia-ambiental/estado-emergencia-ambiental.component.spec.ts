import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoEmergenciaAmbientalComponent } from './estado-emergencia-ambiental.component';

describe('EstadoEmergenciaAmbientalComponent', () => {
  let component: EstadoEmergenciaAmbientalComponent;
  let fixture: ComponentFixture<EstadoEmergenciaAmbientalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoEmergenciaAmbientalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoEmergenciaAmbientalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
