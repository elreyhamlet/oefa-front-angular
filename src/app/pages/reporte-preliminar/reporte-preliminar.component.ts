import { Component, OnInit, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Coordenada, Utm } from '../../interface/coordenadas.interface';
import { ConfirmarModalComponent } from '../../components/confirmar-modal/confirmar-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { filter, repeat, switchMap, tap } from 'rxjs/operators'
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../shared/shared.service';
import { FilesAlfresco } from '../../interface/files.interface';
import { fromLatLon } from '../../helpers/helpers';
import { RPEAService } from '../../services/rpea.service';
import { Rpea, ListaArchivo, EventoRpea, Coordenadas } from '../../interface/rpea.interface';
import { Departamento, Provincia, Distrito } from '../../interface/ubigeo.interface';
import { DomSanitizer } from '@angular/platform-browser';
import { Sector, UnidadFiscalizable, UnidadMedida } from '../../interface/opcionesDesplegables.interface';
import { timer } from 'rxjs';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { AlertaModalComponent } from '../../components/alerta-modal/alerta-modal.component';
import { DatosRfea } from '../../interface/rfea.interface';
import { LocacionService } from '../../services/locacion.service';



interface OrigenEA {
  origen: string;
  checked: boolean;
  descripcion?: string;
  imagen: string;
}
interface ComponenteAfectado {
  nombre: string;
  checked: boolean;
  origen?: string;
  descripcion?: string;
}



@Component({
  selector: 'app-reporte-preliminar',
  templateUrl: './reporte-preliminar.component.html',
  styleUrls: ['./reporte-preliminar.component.css']
})
export class ReportePreliminarComponent implements OnInit {

  @ViewChild('fileVisible', { static: false }) fileVisible!: ElementRef;
  @ViewChild("formUpload", { static: false }) myFormUpload!: ElementRef;
  @ViewChild("formReg", { static: false }) myFormRegister!: ElementRef;
  @ViewChild("divContentInputFile", { static: false }) myDivInputFile!: ElementRef;
  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef;
  idRpea: string = '';
  formularioTab1!: FormGroup;
  formUpload!: FormGroup;
  departamentos: Departamento[] = [];
  provincias: Provincia[] = [];
  distritos: Distrito[] = [];
  sectores: Sector[] = [];
  unidadesMedida: UnidadMedida[] = [];
  unidadesFiscalizables: UnidadFiscalizable[] = [];
  utm: Utm = {
    easting: 0,
    northing: 0,
    zoneLetter: '',
    zoneNum: 0
  };

  hkey = '';
  urlIframe = '';
  idArchivo = 0;
  isLoading = false;
  fileAlfresco!: FilesAlfresco;
  validarFile: any;
  idFile = 0;
  eaID = '';
  flagEnviando = false;
  componenentesAfectadosList: ComponenteAfectado[] = [
    {
      nombre: 'Agua',
      checked: false,
    },
    {
      nombre: 'Suelo',
      checked: false,
    },
    {
      nombre: 'Aire',
      checked: false,
    },
    {
      nombre: 'Otros',
      origen: 'Otros',
      checked: false,
      descripcion: ''
    }
  ];
  files: FilesAlfresco[] = [];
  coordendas!: Coordenada;
  mensajeStatus = 'Enviando datos...';

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router,
    private activated: ActivatedRoute,
    private sharedService: SharedService,
    private wsrpea: RPEAService,
    private sanitizer: DomSanitizer,
    private locacion: LocacionService
  ) {

    this.crearFormularioTAb1();
    this.formUpload = this.formBuilder.group({
      documento: [''],
      hkey: ['537489755']
    });
  }

  ngOnInit() {
    this.cargarDatosALTab1();
    this.activated.queryParams.subscribe((res) => {
      this.idRpea = res.idRPEA ? res.idRPEA : '';
      this.eaID = res.idEA ? res.idEA : '';
      this.incializarDatos();
    });

  }

  incializarDatos() {
    if (this.idRpea !== '' && this.eaID !== '') {
      this.cargarDatosRpea();
    } else {
      this.cargarSelectores();
      this.datosPersonas.push(
        this.formBuilder.group(
          {
            nombreContacto: ['', [Validators.required]],
            correoContacto: ['', [Validators.required]],
            telefonoContacto: ['', [Validators.required]]
          }
        )
      );
    }
  }

  cargarDatosRpea() {

    this.wsrpea.obtenerRPEA({ idRPEA: this.idRpea, idEA: this.eaID }).subscribe((res) => {
      if (res.estado) {

        if (res.payload.administradoRpea) {
          this.cargarDatosPreRegitrados(res.payload);
          this.listarOpcionSector(res.payload.administradoRpea.idSubSector?.toString());
          this.listarUnidadFiscalizable(res.payload.administradoRpea?.idUnidadFiscalizable?.toString());
          this.listarOpcionUnidadMedida(res.payload.eventoRpea?.idUnidadAreaAfectada, res.payload.eventoRpea?.idCantidadInvolucrada);
          this.cargarSelectoresAnidados(res.payload.eventoRpea?.idUbigeo);
          this.cargarArchivosRegitrados(res.payload.administradoRpea?.listaArchivo);
          this.cargarCoordenadas(res.payload.eventoRpea);
          this.cargarUTM(res.payload.eventoRpea?.coordenadas);
        } else {

          this.cargarSelectores();
        }

      }
    });

  }

  cargarSelectores() {
    this.listarOpcionSector();
    this.listarUnidadFiscalizable();
    this.listarOpcionUnidadMedida();
    this.cargarSelectoresAnidados();
    this.cargarCoordenadasDefault();

  }

  cargarCoordenadas(datosEvento: EventoRpea) {

    this.coordendas = {
      lat: +datosEvento.coordenadas.latitud,
      lng: +datosEvento.coordenadas.longitud
    };
  }

  cargarCoordenadasDefault() {
    this.locacion.getPosition().then((r) => {
      this.coordendas = r;
    }).catch(() => {
      const coordenadas = {
        lat: -9.189967,
        lng: -75.015152
      };
      this.coordendas = coordenadas;
    })
  }

  cargarSelectoresAnidados(ubigeo?: string) {
    let arrayUbigeo: any = [];
    if (ubigeo) {
      arrayUbigeo = ubigeo.split("");
    }
    let idDepartamento = '';
    let idProvincia = '';
    let idDistrito = '';
    this.wsrpea.listarDepartamentos().subscribe((res) => {
      this.departamentos = res.payload;
      this.formularioTab1.get('departamento')?.
        valueChanges
        .pipe(
          tap(() => {
            this.formularioTab1.get('provincia')?.reset('');

          }),
          switchMap((dep: string) => this.wsrpea.listProvincias(dep))
        )
        .subscribe((res) => {
          this.provincias = res.payload;
        });
    });

    this.formularioTab1.get('provincia')?.
      valueChanges
      .pipe(
        tap(() => {
          this.distritos = [];
          this.formularioTab1.get('distrito')?.reset('');
        }),
        filter((res) => { return res !== '' }),
        switchMap((prov: string) => this.wsrpea.listarDistritos(prov, this.formularioTab1.get('departamento')?.value))
      )
      .subscribe((res) => {
        this.distritos = res.payload;

      });

    if (arrayUbigeo.length > 0) {
      timer(1000).subscribe((res: any) => {
        idDepartamento = arrayUbigeo[0] + arrayUbigeo[1];
        idProvincia = arrayUbigeo[2] + arrayUbigeo[3];
        idDistrito = arrayUbigeo[4] + arrayUbigeo[5];
        this.formularioTab1.get('departamento')?.setValue(idDepartamento);
        this.formularioTab1.get('provincia')?.setValue(idProvincia);
        this.formularioTab1.get('distrito')?.setValue(idDistrito);
      });



    }
  }

  crearFormularioTAb1() {
    this.formularioTab1 = this.formBuilder.group({
      idUnidadFiscalizable: [''],
      unidadFizcalizable: ['',],
      flagUnidadFiscalizable: [true,],
      idSubSector: [''],
      subSector: ['',],
      contacto: this.formBuilder.array([]),
      detalleEvidencia: [''],
      nombreInstalacion: [''],
      fechaEvento: '',
      horaInicioEvento: [''],
      horaFinEvento: [''],
      areaAfectada: [''],
      idUnidadAreaAfectada: [''],
      unidadAreaAfectada: [''],
      cantidadInvolucrada: [''],
      idCantidadInvolucrada: [''],
      unidadCantidadInvolucrada: [''],
      descripcionLugar: [''],
      coordenadaEste: [''],
      coordenadaNorte: [''],
      departamento: [''],
      provincia: [''],
      distrito: [''],
      localidad: [''],
      zona: [''],
      climaticoFlag: ['',],
      tecnologicoFlag: ['',],
      humanoFlag: ['',],
      terceroFlag: ['',],
      otrosFlag: ['',],
      origenDescripcion: [''],
      descripcionEvento: [''],
      aguaFlag: [''],
      sueloFlag: [''],
      aireFlag: [''],
      otrosFlagCA: [''],
      flagPlanContingencia: [''],
      descripcionPlanContigencia: [''],
      componenteDescripcion: [''],
    });

  }

  get datosPersonas() {
    return this.formularioTab1.get('contacto') as FormArray;
  }

  cargarDatosALTab1() {


  }
  eliminarDatosPersonas(index: number) {
    this.datosPersonas.removeAt(index);
  }

  guardarDatosEA() {
    console.log(this.obtenerDatosForm());
  }

  obtenerDatosForm() {

    return {
      eaId: this.eaID,
      administradoRpea: {
        idUnidadFiscalizable: this.formularioTab1.get('unidadFizcalizable')?.value,
        unidadFiscalizable: this.obtenerNoombreUnidadFizcalizable(this.formularioTab1.get('unidadFizcalizable')?.value)?.unidadFiscalizableDescripcion,
        flagUnidadFiscalizable: this.formularioTab1.get('flagUnidadFiscalizable')?.value,
        idSubSector: this.formularioTab1.get('subSector')?.value,
        subSector: this.formularioTab1.get('subSector')?.value,
        contacto: this.formularioTab1.get('contacto')?.value,
        detalleEvidencia: this.formularioTab1.get('detalleEvidencia')?.value,
        nombreInstalacion: this.formularioTab1.get('nombreInstalacion')?.value,
        listaArchivo: this.obtenerUUIDfile()
      },
      eventoRpea: {
        nombreInstalacion: this.formularioTab1.get('nombreInstalacion')?.value,
        fechaEvento: this.formularioTab1.get('fechaEvento')?.value,
        horaInicioEvento: this.formularioTab1.get('horaInicioEvento')?.value,
        horaFinEvento: this.formularioTab1.get('horaFinEvento')?.value,
        areaAfectada: this.formularioTab1.get('areaAfectada')?.value,
        idUnidadAreaAfectada: this.formularioTab1.get('unidadAreaAfectada')?.value,
        unidadAreaAfectada: this.obtenerNoombreUnidadAreaAfectada(this.formularioTab1.get('unidadAreaAfectada')?.value)?.unidadMedidaDescripcion,
        cantidadInvolucrada: this.formularioTab1.get('cantidadInvolucrada')?.value,
        idCantidadInvolucrada: this.formularioTab1.get('unidadCantidadInvolucrada')?.value,
        unidadCantidadInvolucrada: this.obtenerNoombreUnidadAreaAfectada(this.formularioTab1.get('unidadCantidadInvolucrada')?.value)?.unidadMedidaDescripcion,
        descripcionLugar: this.formularioTab1.get('descripcionLugar')?.value,
        coordenadas: this.obtenerCoordenadas(),
        ubigeo: this.obtenerDatosUbigeo(),
        localidad: this.formularioTab1.get('localidad')?.value,
        zona: this.formularioTab1.get('zona')?.value,
      },
      origenRpea: {
        origenEA: this.obtenerOrigenEA(),
        descripcionEvento: this.formularioTab1.get('descripcionEvento')?.value,
        componenteEA: this.obtenerComponenteEA(),
        flagPlanContingencia: this.formularioTab1.get('flagPlanContingencia')?.value,
        descripcionPlanContigencia: this.formularioTab1.get('descripcionPlanContigencia')?.value,
      },

    }
  }

  obtenerNoombreUnidadFizcalizable(id: string) {
    return this.unidadesFiscalizables.find((r) => r.unidadFiscalizableId === id)
  }
  obtenerNoombreUnidadAreaAfectada(id: number) {
    return this.unidadesMedida.find((r) => r.unidadMedidaId === id)
  }
  obtenerOrigenEA() {
    return {
      climaticoFlag: this.formularioTab1.get('climaticoFlag')?.value,
      tecnologicoFlag: this.formularioTab1.get('tecnologicoFlag')?.value,
      humanoFlag: this.formularioTab1.get('humanoFlag')?.value,
      terceroFlag: this.formularioTab1.get('terceroFlag')?.value,
      otrosFlag: this.formularioTab1.get('otrosFlag')?.value,
      origenDescripcion: this.formularioTab1.get('origenDescripcion')?.value,
    };
  }

  obtenerComponenteEA() {
    return {
      aguaFlag: this.formularioTab1.get('aguaFlag')?.value,
      sueloFlag: this.formularioTab1.get('sueloFlag')?.value,
      aireFlag: this.formularioTab1.get('aireFlag')?.value,
      otrosFlag: this.formularioTab1.get('otrosFlag')?.value,
      componenteDescripcion: this.formularioTab1.get('componenteDescripcion')?.value,
    };
  }

  obtenerCredencialesAlfresco(body: any) {
    return this.wsrpea.obtenerCredencialesAlfresco(body);
  }



  volver() {
    this.router.navigate(['/']);
  }
  limpiar() {
    this.formularioTab1.reset(
      {
        idUnidadFiscalizable: '',
        unidadFizcalizable: '',
        flagUnidadFiscalizable: true,
        idSubSector: '',
        subSector: '',
        contacto: [],
        detalleEvidencia: '',
        nombreInstalacion: '',
        fechaEvento: '',
        horaInicioEvento: '',
        horaFinEvento: '',
        areaAfectada: '',
        idUnidadAreaAfectada: '',
        unidadAreaAfectada: '',
        cantidadInvolucrada: '',
        idCantidadInvolucrada: '',
        unidadCantidadInvolucrada: '',
        descripcionLugar: '',
        coordenadaEste: '',
        coordenadaNorte: '',
        idDepartamento: '',
        departamento: '',
        idprovincia: '',
        provincia: '',
        idDistrito: '',
        distrito: '',
        localidad: '',
        zona: '',
        climaticoFlag: '',
        tecnologicoFlag: '',
        humanoFlag: '',
        terceroFlag: '',
        otrosFlag: '',
        origenDescripcion: '',
        descripcionEvento: '',
        flagPlanContingencia: '',
        descripcionPlanContigencia: '',
        aguaFlag: '',
        sueloFlag: '',
        aireFlag: '',
        otrosFlagCA: '',
        componenteDescripcion: ''
      }
    );
  }

  guardarReportePreliminar() {

    const dialogRef = this.dialog.open(ConfirmarModalComponent, {
      width: '700px',
      height: 'auto',
      panelClass: 'mat-sgea-dialog',

      data: { ...this.obtenerDatosForm() }
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {

      }
    });

  }



  pintarCoordenadas(coords: Coordenada) {
    this.coordendas = coords;
    const utm = fromLatLon(this.coordendas.lat, this.coordendas.lng);
    this.utm = utm;
    this.formularioTab1.get('coordenadaEste')?.setValue(Math.trunc(this.utm.easting));
    this.formularioTab1.get('coordenadaNorte')?.setValue(Math.trunc(this.utm.northing));

  }

  cargarUTM(item: Coordenadas) {
    this.utm = {
      easting: +item.coordenadaEste,
      northing: +item.coordenadaNorte,
      zoneLetter: item.zonaLetra,
      zoneNum: +item.zonaNumero
    };
  }
  updateOA(item: MatCheckboxChange) {
    if (!item.checked) {
      this.formularioTab1.get('origenDescripcion')?.setValue('');
    }

  }
  updateCA(item: MatCheckboxChange) {

    if (!item.checked) {
      this.formularioTab1.get('componenteDescripcion')?.setValue('');
    }


  }

  obtenerUUIDfile() {
    const filtrarFiles = this.files
      .filter(r => r.status === true)
      .map((r) => {
        return {
          uuidArchivo: r.uuid
        }
      });
    return filtrarFiles;

  }

  obtenerCoordenadas() {



    return {
      coordenadaEste: Math.trunc(this.utm.easting),
      coordenadaNorte: Math.trunc(this.utm.northing),
      zonaLetra: this.utm.zoneLetter,
      zonaNumero: this.utm.zoneNum,
      longitud: this.coordendas.lng,
      latitud: this.coordendas.lat

    }
  }
  obtenerDatosUbigeo() {
    return {
      departamento: this.formularioTab1.get('departamento')?.value,
      provincia: this.formularioTab1.get('provincia')?.value,
      distrito: this.formularioTab1.get('distrito')?.value,
    }
  }

  cargarDatosPreRegitrados(rpea: Rpea) {
    let date: any[] = [];
    let hoursInicio: any[] = [];
    let hoursFin: any[] = [];

    let fechaEvento = '';
    let horaInicioEvento = '';
    let horaFinEvento = '';

    if (rpea.eventoRpea.fechaEvento) {
      date = rpea.eventoRpea.fechaEvento.split(" ");
      fechaEvento = date[0];

    }

    if (rpea.eventoRpea.horaInicioEvento) {
      hoursInicio = rpea.eventoRpea.horaInicioEvento.split(" ");
      horaInicioEvento = hoursInicio[1];
    }

    if (rpea.eventoRpea.horaFinEvento) {

      hoursFin = rpea.eventoRpea.horaFinEvento.split(" ");
      horaFinEvento = hoursFin[1];
    }





    this.formularioTab1.reset(
      {
        idUnidadFiscalizable: rpea.administradoRpea.idUnidadFiscalizable,
        unidadFizcalizable: rpea.administradoRpea.unidadFiscalizable,
        flagUnidadFiscalizable: rpea.administradoRpea.flagUnidadFiscalizable,
        idSubSector: rpea.administradoRpea.idSubSector,
        subSector: rpea.administradoRpea.subSector,
        detalleEvidencia: rpea.administradoRpea.detalleEvidencia,
        nombreInstalacion: rpea.eventoRpea.nombreInstalacion,
        fechaEvento: fechaEvento, // cambiar fecha formato
        horaInicioEvento: horaInicioEvento,
        horaFinEvento: horaFinEvento,
        areaAfectada: rpea.eventoRpea.areaAfectada,
        idUnidadAreaAfectada: rpea.eventoRpea.idUnidadAreaAfectada,
        unidadAreaAfectada: rpea.eventoRpea.unidadAreaAfectada,
        cantidadInvolucrada: rpea.eventoRpea.cantidadInvolucrada,
        idCantidadInvolucrada: rpea.eventoRpea.idCantidadInvolucrada,
        unidadCantidadInvolucrada: rpea.eventoRpea.unidadCantidadInvolucrada,
        descripcionLugar: rpea.eventoRpea.descripcionLugar,
        coordenadaEste: rpea.eventoRpea.coordenadas.coordenadaEste,
        coordenadaNorte: rpea.eventoRpea.coordenadas.coordenadaNorte,
        localidad: rpea.eventoRpea.localidad,
        zona: rpea.eventoRpea.zona,
        climaticoFlag: rpea.origenRpea.origenEA.climaticoFlag,
        tecnologicoFlag: rpea.origenRpea.origenEA.tecnologicoFlag,
        humanoFlag: rpea.origenRpea.origenEA.humanoFlag,
        terceroFlag: rpea.origenRpea.origenEA.terceroFlag,
        otrosFlag: rpea.origenRpea.origenEA.otrosFlag,
        origenDescripcion: rpea.origenRpea.origenEA.origenDescripcion,
        descripcionEvento: rpea.origenRpea.descripcionEvento,
        aguaFlag: rpea.origenRpea.componenteEA.aguaFlag,
        sueloFlag: rpea.origenRpea.componenteEA.sueloFlag,
        aireFlag: rpea.origenRpea.componenteEA.aireFlag,
        otrosFlagCA: rpea.origenRpea.componenteEA.otrosFlag,
        componenteDescripcion: rpea.origenRpea.componenteEA.componenteDescripcion,
        flagPlanContingencia: rpea.origenRpea.flagPlanContingencia,
        descripcionPlanContigencia: rpea.origenRpea.descripcionPlanContigencia,

      }
    );

    if (rpea.administradoRpea?.contacto?.length > 0) {
      for (const item of rpea.administradoRpea.contacto) {
        this.datosPersonas.push(
          this.formBuilder.group(
            {
              nombreContacto: [item?.nombreContacto, Validators.required],
              correoContacto: [item?.correoContacto, Validators.required],
              telefonoContacto: [item?.telefonoContacto, Validators.required]
            }
          )
        );
      }
    } else {
      this.datosPersonas.push(
        this.formBuilder.group(
          {
            nombreContacto: ['', Validators.required],
            correoContacto: ['', Validators.required],
            telefonoContacto: ['', Validators.required]
          }
        )
      );
    }


  }
  agregarNuevoDatosPersonales() {
    this.datosPersonas.push(this.formBuilder.group(
      {
        nombreContacto: ['', [Validators.required]],
        correoContacto: ['', [Validators.required]],
        telefonoContacto: ['', [Validators.required]]
      }
    ));
  }

  onFileChanged(event: any) {
    this.isLoading = true;
    const file = event.target.files[0];
    const files = [...this.files];
    if (file) {
      const ok = this.sharedService.validarArchivoPermitido(file, files);
      this.validarFile = ok;
      if (ok.estado) {
        const data = {
          tipoArchivo: "MPV",
          pathAlfresco: "file.path.compromiso.ambiental"
        };
        this.obtenerCredencialesAlfresco(data).subscribe((res: any) => {
          if (res.estado) {
            const fileAlgresco: FilesAlfresco = ok.payload;
            const idarchivo = res.idArchivo;
            const path = res.formActionURL;
            const key = res.keyId;
            this.urlIframe = path;
            this.hkey = key;
            this.idArchivo = idarchivo;
            this.fileAlfresco = fileAlgresco;
            setTimeout(() => {
              this.myFormUpload.nativeElement.appendChild(this.myFormRegister.nativeElement.querySelector('input[type=file]'));
              this.myFormUpload.nativeElement.submit();
            }, 500);
          } else {
            event.target.value = '';
            this.fileVisible.nativeElement.value = '';
            this.isLoading = false;
          }
        });
      } else {
        event.target.value = '';
        this.fileVisible.nativeElement.value = '';
        this.isLoading = false;
      }
    } else {
      event.target.value = '';
      this.fileVisible.nativeElement.value = '';
      this.isLoading = false;
    }
  }

  iframeLoaded() {
    if (this.idArchivo === 0) {
      return;
    }
    this.myDivInputFile.nativeElement.appendChild(this.fileInput.nativeElement);
    this.fileVisible.nativeElement.value = '';
    this.fileInput.nativeElement.value = '';
    this.wsrpea.obtenerArchivoRegistrado(this.idArchivo).subscribe((res: any) => {

      this.fileAlfresco.uuid = res.uuid;
      this.fileAlfresco.status = true;
      this.isLoading = false;
      this.fileAlfresco.name = res?.nombreArchivo;
      this.files.push({ ...this.fileAlfresco });


    }, () => {
      this.isLoading = false;
    });


  }
  iframeURL(url: any) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  getIframeHTML() {
    const element: HTMLIFrameElement = document.getElementById('uploadiframe') as HTMLIFrameElement;
    let algo = element.contentDocument ? element.contentDocument : element.contentWindow;
  }
  listarOpcionSector(subSector?: string) {

    this.wsrpea.listarSector().subscribe((res) => {
      if (res.estado) {
        this.sectores = res.payload;
        if (subSector) {
          this.formularioTab1.get('subSector')?.setValue(subSector);
        }
      }
    });
  }
  listarUnidadFiscalizable(idUnidadFiscalizable?: string) {

    this.wsrpea.listarOpcionUnidadFiscalizable().subscribe((res) => {
      if (res.estado) {
        this.unidadesFiscalizables = res.payload;
        if (idUnidadFiscalizable) {
          this.formularioTab1.get('unidadFizcalizable')?.setValue(idUnidadFiscalizable);
        }
      }

    });


  }


  listarOpcionUnidadMedida(unidadMedida?: any, unidadCatidad?: any) {
    this.wsrpea.listarUnidadMedida().subscribe((res) => {
      if (res.estado) {
        this.unidadesMedida = res.payload;
        this.formularioTab1.get('unidadAreaAfectada')?.setValue(unidadMedida ? unidadMedida : this.unidadesMedida[0].unidadMedidaId);
        this.formularioTab1.get('unidadCantidadInvolucrada')?.setValue(unidadCatidad ? unidadCatidad : this.unidadesMedida[0].unidadMedidaId);

      }

    });
  }

  descargarArchivoGenerado(item: FilesAlfresco) {
    this.wsrpea.descargarArchivo(item.uuid).subscribe((res) => {

      const a = document.createElement('a');
      a.href = res.fullUrlDownload;
      a.target = '_BLANK';
      a.click();

    });
  }

  cargarArchivosRegitrados(item: ListaArchivo[]) {


    item.forEach((r, i) => {

      if (r) {

        r.nombreArchivo = r?.nombreArchivo ? r?.nombreArchivo : 'documento_sin_nombre_' + (i + 1),
          this.files.push(
            {
              name: r?.nombreArchivo,
              uuid: r?.uuidArchivo,
              size: '',
              status: true,

            }
          );
      }

    });
  }


  guardarPresntacionDialog(mensaje: string) {
    const dialogRef = this.dialog.open(AlertaModalComponent, {
      width: '700px',
      height: 'auto',
      panelClass: 'mat-sgea-dialog',
      data: mensaje
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {

      }
    });

  }

  registrarPresentacion() {
    this.flagEnviando = true;
    this.wsrpea.registrarPresentacion({ eaId: this.eaID }).subscribe((res: any) => {
      this.flagEnviando = false;
      this.guardarPresntacionDialog(res.mensaje);

    }, () => {
      this.flagEnviando = false;
      this.guardarPresntacionDialog('Hubo un error al enviar los datos');

    });
  }


  registrarRPEA() {
    this.flagEnviando = true;
    const rpea = this.obtenerDatosForm();



    this.wsrpea.registrarRPEA({ ...rpea }).subscribe((res: any) => {
      this.flagEnviando = false;
      this.guardarPresntacionDialog(res.mensaje);
    }, () => {
      this.flagEnviando = false;
      this.guardarPresntacionDialog('Hubo un error al enviar los datos');
    });
  }
}
