import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BandejaEmergenciaAmbientalComponent } from './bandeja-emergencia-ambiental.component';

describe('BandejaEmergenciaAmbientalComponent', () => {
  let component: BandejaEmergenciaAmbientalComponent;
  let fixture: ComponentFixture<BandejaEmergenciaAmbientalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BandejaEmergenciaAmbientalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BandejaEmergenciaAmbientalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
