import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BandejaInterface, BandejaRespone } from '../../interface/bandeja.interface';
import { BandejaService } from '../../services/bandeja.service';
import { Router } from '@angular/router';
import { DialogCoordenada } from '../../interface/coordenadas.interface';
import { DialogMapaComponent } from '../../components/dialog-mapa/dialog-mapa.component';
import { MatPaginatorIntl, PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-bandeja-emergencia-ambiental',
  templateUrl: './bandeja-emergencia-ambiental.component.html',
  styleUrls: ['./bandeja-emergencia-ambiental.component.css']
})
export class BandejaEmergenciaAmbientalComponent extends MatPaginatorIntl
  implements OnInit {
  query = '';
  emergenciaRelacionada = '';
  resultado = '';
  panelOpenState = 0;
  emergenciaAmbientalList: BandejaInterface[] = [];
  emergenciaAmbientalListTemp: BandejaInterface[] = [];
  itemPanelEA!: BandejaInterface;

  length = 0;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  constructor(
    public dialog: MatDialog,
    private bandejaws: BandejaService,
    private router: Router,

  ) {
    super();
    this.itemsPerPageLabel = 'Emergencias por página:';

  }

  calcularPaginacion() {
    this.length = this.emergenciaAmbientalList.length;

  }

  ngOnInit(): void {
    this.listarBandeja('');
  }
  abrirPestana(item: BandejaInterface) {
    for (const iterator of this.emergenciaAmbientalList) {
      if (item?.eaId !== this.itemPanelEA?.eaId) {
        iterator.panel = false;
      }

    }
    item.panel = !item.panel;
    this.itemPanelEA = item;


  }

  abrirMapa(item: BandejaInterface) {
    if (item.emergenciaRelacionada === '') {
      const coord: DialogCoordenada = {
        coordenadas: {
          lat: +item.coordenada.latitud,
          lng: +item.coordenada.longitud
        },
        alturaDialog: '500px'
      };
      console.log(coord)
      const dialogRef = this.dialog.open(DialogMapaComponent, {
        width: '600px',
        height: '600px',
        data: coord
      });

      dialogRef.afterClosed().subscribe((result: any) => {

      });
    }
  }


  listarBandeja(query: string) {

    this.bandejaws.listarEmergenciasAmbientales({ query })
      .subscribe((res: BandejaRespone) => {

        this.emergenciaAmbientalList = res.payload;
        this.calcularPaginacion();
      });
  }


  listarEAporQuery() {
    const query = this.query.trim();
    this.bandejaws.buscarEmergenciasAmbientales({ codigoEmergencia: query })
      .subscribe((res: BandejaRespone) => {
        this.emergenciaAmbientalList = res.payload;
        this.calcularPaginacion();
      });
  }
  relacionarEmergencia(item: BandejaInterface, event: HTMLInputElement) {
    item.mensajeError = '';
    item.mensajeSuccess = '';
    const body = {
      codigoEA: item.codigoEA,
      codigoRelacionEA: event.value.trim()
    };
    this.bandejaws.relacionarEmergencia(body).subscribe((res) => {
      if (res.estado) {
        const eaId = res.payload.eaId;
        if (+eaId === +item.eaId) {

          item.emergenciaRelacionada = body.codigoRelacionEA;
          item.mensajeSuccess = res.mensaje;
        }
      } else {
        item.mensajeError = res.mensaje;
      }

    }, (e) => {

    });
  }

  irEstadoEmergenciaAmbiental(item: BandejaInterface) {

    if (item.emergenciaRelacionada === '') {
      this.router.navigate(['/estado', item.eaId]);
    }

  }

  irReportePreliminar(item: BandejaInterface) {
    if (item.emergenciaRelacionada === '') {
      this.router.navigate(['/reporte-pre'], {
        queryParams: {
          idRPEA: item.idRPEA,
          idEA: item.eaId,
        }
      });
    }

  }

  crearReportePreliminar() {

    this.router.navigate(['/reporte-pre']);


  }
  irReporteFinal(item: BandejaInterface) {
    if (item.emergenciaRelacionada === '') {

      this.router.navigate(['/reporte-fin'], {
        queryParams: {
          idRFEA: item.idRFEA,
          idEA: item.eaId,
        }
      });
    }
  }


  a(event: PageEvent) {
    console.log(event)
  }
  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return '0 de ' + length;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;
    return startIndex + 1 + ' - ' + endIndex + ' de ' + length;
  };
}
