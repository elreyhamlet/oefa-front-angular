
import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-alerta-modal',
  templateUrl: './alerta-modal.component.html',
  styleUrls: ['./alerta-modal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AlertaModalComponent implements OnInit {
  mensaje = '';
  constructor(public dialogRef: MatDialogRef<AlertaModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,


  ) { }


  ngOnInit(): void {

  }

  cerrar() {
    this.dialogRef.close();
  }

}
