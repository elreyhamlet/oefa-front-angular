export const DATA = {
    "estado": true,
    "status": 200,
    "mensaje": "OK",
    "payload": {
        "administradoRpea": {
            "unidadFiscalizable": "hidrocarburos",
            "idUnidadFiscalizable": 1,
            "flagUnidadFiscalizable": true,
            "subSector": "Gas natural",
            "idSubSector": 1,
            "contacto": [
                {
                    "nombreContacto": "Juanichi Basilio Florez",
                    "correoContacto": "jota@gmail.com",
                    "telefonoContacto": "987654321"
                },
                {
                    "nombreContacto": "Christian Basilio Florez",
                    "correoContacto": "cc@gmail.com",
                    "telefonoContacto": "987653621"
                }
            ],
            "listaArchivo": [
                {
                    "uuidArchivo": "33f3dcab-8b68-48ca-ab46-d33e081a6d6f"
                },
                {
                    "uuidArchivo": "c33c9bc9-f3d6-436e-8afa-274e29a8fccb"
                }
            ],
            "detalleEvidencia": "Sucedio algo imprevisto nadie se lo esperaba"
        },
        "eventoRpea": {
            "nombreInstalacion": "Instalación eléctrica",
            "fechaEvento": "17/02/2021",
            "horaInicioEvento": "18:23:54",
            "horaFinEvento": "21:20:00",
            "areaAfectada": "200.40",
            "unidadAreaAfectada": "m2",
            "idUnidadAreaAfectada": 1,
            "cantidadInvolucrada": "150",
            "unidadCantidadInvolucrada": "m2",
            "idCantidadInvolucrada": 1,
            "descripcionLugar": "Fue cerca a la represa hidroeléctrica",
            "coordenadas": {
                "coordenadaEste": "67,40",
                "coordenadaNorte": "57,40",
                "zonaLetra": "57,40",
                "zonaNumero": "57,40",
                "longitud": "57,40",
                "latitud": "57,40"
            },
            "ubigeo": {
                "departamento": "Lima",
                "idDepartamento": 1,
                "provincia": "Lima",
                "idprovincia": 1,
                "distrito": "Breña",
                "idDistrito": 15
            },
            "localidad": "Centro",
            "zona": "Cercado"
        },
        "origenRpea": {
            "origenEA": [
                {
                    "origen": "Climático"
                },
                {
                    "origen": "Tecnológico"
                },
                {
                    "origen": "Humano"
                },
                {
                    "origen": "Tercero"
                },
                {
                    "origen": "Otro",
                    "descripcion": "Aquí va la descripción"
                }
            ],
            "componenteEA": [
                {
                    "componente": "Agua"
                },
                {
                    "componente": "Suelo"
                },
                {
                    "componente": "Aire"
                },
                {
                    "componente": "Otro",
                    "descripcion": "Aquí va la descripción"
                }
            ],
            "descripcionEvento": "se quemó todo",
            "flagPlanContingencia": true,
            "descripcionPlanContigencia": "Se aplico la estrategia"
        }
    }
}