export const DATA_BANDEJA = {
    "estado": true,
    "status": 200,
    "mensaje": "OK",
    "payload": [{
        "sectorEA": "Mineria",
        "codigoEA": "2021-EA01-00001",
        "fechaRegistroEA": "2021-05-05",
        "nombreCoordinador": "Dirección de supervisión ambiental en energía y minas",
        "unidadFiscalizable": "Hidrocarburos",
        "fechaEA": "2021-05-05",
        "fechaRegistroRPEA": "2021-05-05",
        "fechaRegistroRFEA": "2021-05-05",
        "idRPEA": 13,
        "idRFEA": 13,
        "estadoRPEA": "Completado",
        "estadoRFEA": "Pendiente",
        "estadoEA": "Registro en el sistema",
        "coordenadas": {
            "este": "14",
            "norte": "21",
            "latitud": "45",
            "longitud": "42"
        }
    },
    {
        "sectorEA": "Mineria",
        "codigoEA": "2021-ST01-00001",
        "fechaRegistroEA": "2021-05-05",
        "nombreCoordinador": "Dirección de supervisión ambiental en energía y minas",
        "unidadFiscalizable": "Hidrocarburos",
        "fechaEA": "2021-05-05",
        "fechaRegistroRPEA": "2021-05-05",
        "fechaRegistroRFEA": "2021-05-05",
        "idRPEA": 13,
        "idRFEA": 13,
        "estadoRPEA": "Completado",
        "estadoRFEA": "Pendiente",
        "estadoEA": "Registro de RPEA",
        "coordenadas": {
            "este": "14",
            "norte": "21",
            "latitud": "45",
            "longitud": "42"
        }
    },
    {
        "sectorEA": "Mineria",
        "codigoEA": "2021-FG01-00001",
        "fechaRegistroEA": "2021-05-05",
        "nombreCoordinador": "Dirección de supervisión ambiental en energía y minas",
        "unidadFiscalizable": "Hidrocarburos",
        "fechaEA": "2021-05-05",
        "fechaRegistroRPEA": "2021-05-05",
        "fechaRegistroRFEA": "2021-05-05",
        "idRPEA": 13,
        "idRFEA": 13,
        "estadoRPEA": "Completado",
        "estadoRFEA": "Pendiente",
        "estadoEA": "Registro de RFEA",
        "coordenadas": {
            "este": "14",
            "norte": "21",
            "latitud": "45",
            "longitud": "42"
        }
    }
    ]
}