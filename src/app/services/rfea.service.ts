import { Injectable } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { HttpHeaders } from '@angular/common/http';
import { WS_MOSTRAR_RFEA, WS_REGISTRAR_PRESENTACION_RFEA, WS_REGISTRAR_RPEA, WS_REGISTRAR_RFEA } from '../shared/config';
import { RfeaResponse } from '../interface/rfea.interface';
import { RPEAService } from './rpea.service';

@Injectable({
  providedIn: 'root'
})
export class RfeaService {

  constructor(private shared: SharedService, private wsrpea: RPEAService) { }

  obtenerRFEA(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`
    });
    return this.shared.postQuery<RfeaResponse>(WS_MOSTRAR_RFEA, body, headers);
  }

  listarUnidadMedida() {
    return this.wsrpea.listarUnidadMedida();
  }
  registrarPresentacionrfea(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<any>(WS_REGISTRAR_PRESENTACION_RFEA, body, headers);
  }

  registrarRFEA(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<any>(WS_REGISTRAR_RFEA, body, headers);
  }
}
