import { TestBed } from '@angular/core/testing';

import { BandejaService } from './bandeja.service';

describe('BandejaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BandejaService = TestBed.get(BandejaService);
    expect(service).toBeTruthy();
  });
});
