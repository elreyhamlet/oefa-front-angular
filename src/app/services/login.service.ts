import { Injectable } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { HttpHeaders } from '@angular/common/http';
import { WS_AUTOLOGIN } from '../shared/config';

@Injectable({
  providedIn: 'root'
})
export class LoginService {


  constructor(private shared: SharedService) { }


  autologin(token: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer  ${this.shared.getToken}`
    });

    return this.shared.postQuery<any>(`${WS_AUTOLOGIN}/${token}`, null, headers);
  }

  autologinDni(dni: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer  ${this.shared.getToken}`
    });

    return this.shared.postQuery<any>(`${WS_AUTOLOGIN}/dni/${dni}`, null, headers);
  }
}
