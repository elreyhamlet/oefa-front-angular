import { TestBed } from '@angular/core/testing';

import { RfeaService } from './rfea.service';

describe('RfeaService', () => {
  let service: RfeaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RfeaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
