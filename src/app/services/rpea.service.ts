import { Injectable } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { HttpHeaders } from '@angular/common/http';
import {
  WS_OBTENER_CREDENCIALES,
  WS_OBTENER_ARCHIVOS_REGISTRADOS,
  WS_MOSTRAR_RPEA,
  WS_LISTAR_DEPARTAMENTOS,
  WS_LISTAR_PROVINCIAS,
  WS_LISTAR_DISTRITOS,
  WS_REGISTRAR_RFEA,
  WS_REGISTRAR_RPEA
} from '../shared/config';
import { RpeaResponse } from '../interface/rpea.interface';
import { DepartamentoResponse, ProvinciaResponse, DistritoResponse } from '../interface/ubigeo.interface';
import { WS_LISTAR_SECTOR, WS_LISTAR_UNIDAD_FISCALIZABLE, WS_LISTAR_UNIDAD_MEDIDA, WS_DESCARGAR_ARCHIVO, WS_REGISTRAR_PRESENTACION_RPEA } from '../shared/config';
import { SectorResponse, UnidadFiscalizableResponse, UnidadMedidaResponse } from '../interface/opcionesDesplegables.interface';
import { ArchivoAdjunto } from '../interface/archivoAdjunto.interface';

@Injectable({
  providedIn: 'root'
})
export class RPEAService {

  constructor(private shared: SharedService) { }


  listarDepartamentos() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<DepartamentoResponse>(WS_LISTAR_DEPARTAMENTOS, null, headers);
  }

  listProvincias(idDepartamento: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`
    });

    return this.shared.postQuery<ProvinciaResponse>(`${WS_LISTAR_PROVINCIAS}?idDepartamento=${idDepartamento}`, null, headers);
  }

  listarDistritos(idProvincia: string, idDepartamento: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`
    });

    return this.shared.postQuery<DistritoResponse>(`${WS_LISTAR_DISTRITOS}?idProvincia=${idProvincia}&idDepartamento=${idDepartamento}`, null, headers);
  }

  obtenerRPEA(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`
    });
    return this.shared.postQuery<RpeaResponse>(WS_MOSTRAR_RPEA, body, headers);
  }
  obtenerCredencialesAlfresco(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery(WS_OBTENER_CREDENCIALES, body, headers);
  }
  obtenerArchivoRegistrado(idarchivo: number) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.getQuery(`${WS_OBTENER_ARCHIVOS_REGISTRADOS}/${idarchivo}`, headers);
  }

  listarSector() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<SectorResponse>(WS_LISTAR_SECTOR, null, headers);
  }

  listarOpcionUnidadFiscalizable() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<UnidadFiscalizableResponse>(WS_LISTAR_UNIDAD_FISCALIZABLE, null, headers);
  }

  listarUnidadMedida() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<UnidadMedidaResponse>(WS_LISTAR_UNIDAD_MEDIDA, null, headers);
  }

  descargarArchivo(uuid: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<ArchivoAdjunto>(`${WS_DESCARGAR_ARCHIVO}/${uuid}`, null, headers);
  }

  registrarRPEA(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<any>(WS_REGISTRAR_RPEA, body, headers);
  }
  registrarPresentacion(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.shared.getToken}`

    });
    return this.shared.postQuery<any>(WS_REGISTRAR_PRESENTACION_RPEA, body, headers);
  }
}
