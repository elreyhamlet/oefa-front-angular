import { Injectable } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { HttpHeaders } from '@angular/common/http';
import { BandejaRespone } from '../interface/bandeja.interface';
import { WS_LISTAR_EA, WS_RELACIONAR_EMERGENCIA, WS_BUSCAR_EA } from '../shared/config';

@Injectable({
  providedIn: 'root'
})
export class BandejaService {

  constructor(private service: SharedService) { }
  listarEmergenciasAmbientales(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.service.getToken}`
    });
    /* return of(DATA_BANDEJA); */
    return this.service.postQuery<BandejaRespone>(WS_LISTAR_EA, body, headers);
  }

  relacionarEmergencia(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.service.getToken}`
    });
    /* return of(DATA_BANDEJA); */
    return this.service.postQuery<any>(WS_RELACIONAR_EMERGENCIA, body, headers);
  }
  buscarEmergenciasAmbientales(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.service.getToken}`
    });
    /* return of(DATA_BANDEJA); */
    return this.service.postQuery<BandejaRespone>(WS_BUSCAR_EA, body, headers);
  }


}
