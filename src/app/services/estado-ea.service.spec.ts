import { TestBed } from '@angular/core/testing';

import { EstadoEAService } from './estado-ea.service';

describe('EstadoEAService', () => {
  let service: EstadoEAService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoEAService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
