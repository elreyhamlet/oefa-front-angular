import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { SharedService } from '../shared/shared.service';
import { BandejaInterface } from '../interface/bandeja.interface';
import { EstadoEAResponse } from '../interface/estadoEA.interface';
import { WS_LISTAR_ESTADOS } from '../shared/config';

@Injectable({
  providedIn: 'root'
})
export class EstadoEAService {


  constructor(private service: SharedService) { }
  listarEstadosEA(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.service.getToken}`
    });

    return this.service.postQuery<EstadoEAResponse>(WS_LISTAR_ESTADOS, body, headers);
  }



}
